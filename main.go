/*
Resolving package dependencies

Assignment:
		For a list of packages (A,B,C,..etc) with their corresponding package dependencies:

		A:D,B
		B:C,E
		C:D,E

		provide a list of dependencies in installation order.

Personal thoughts:
The data structure of the assignment, I would consider a graph. The graph
describes the interdependencies between packages and may be a cyclic graph.
Sorting of the nodes in the graph can be done using topological sorting where
I am using Depth-first search (DFS) but only for a subgraph of the original
graph. Pseudo code (copied from Wikipedia) has been added below in the
topSortDFSwithTarget function. I wanted to implement the dependency list without
using external libraries/packages.

Inspiration/Shameful Copy Paste:
https://en.wikipedia.org/wiki/Topological_sorting
https://rosettacode.org/wiki/Topological_sort#cite_note-1
https://rosettacode.org/wiki/Topological_sort/Extracted_top_item#Go
https://www.electricmonk.nl/log/2008/08/07/dependency-resolving-algorithm/
https://dnaeon.github.io/dependency-graph-resolution-algorithm-in-go/
*/

package main

import (
	"fmt"
)

type graph map[string][]string

func main() {

	// Create graph with dependencies
	var g graph
	g = graph{
		"a": []string{"b", "d"},
		"b": []string{"c", "e"},
		"c": []string{"d", "e"},
		"d": []string{},
		"e": []string{},
	}

	// Set the package for which we want to find dependencies
	targetPackage := "a"

	result, cyclic := g.topSortDFSwithTarget(targetPackage)
	if cyclic != nil {
		reverse(cyclic)
		fmt.Println("For package:", targetPackage, "the following (cyclic) dependencies must be installed in this order:", cyclic)

	} else {
		reverse(result)
		fmt.Println("For package:", targetPackage, "the following dependencies must be installed in this order:", result)
	}
}

/*
topSortDFSwithTarget creates a topological ordering of a subgraph based on
start parameter. The function returns either a sorted list where no cyclic
dependencies have occured, or a list of packages where are cyclic dependency has
appeared
*/
func (g graph) topSortDFSwithTarget(start ...string) (order, cyclic []string) {

	// Empty list that will contain the sorted nodes
	sortedNodes := make([]string, len(g))

	numberSortedNodes := len(sortedNodes)
	// Var names based on pseudo code for DFS from Wikipedia
	temporary := map[string]bool{}
	permanent := map[string]bool{}

	var cycleFound bool
	var cycleStart string
	var visit func(string)

	/*
		Pseudo code from Wikipedia
			function visit(node n)
			  if n has a permanent mark then return
			  if n has a temporary mark then stop   (not a DAG)
			  mark n temporarily
			  for each node m with an edge from n to m do
			    visit(m)
			  mark n permanently
			  add n to head of L
	*/
	visit = func(n string) {
		switch {
		case temporary[n]:
			cycleFound = true
			cycleStart = n
			return
		case permanent[n]:
			return
		}
		temporary[n] = true
		for _, m := range g[n] {
			visit(m)
			if cycleFound {
				if cycleStart > "" {
					cyclic = append(cyclic, n)
					if n == cycleStart {
						cycleStart = ""
					}
				}
				return
			}
		}
		delete(temporary, n)
		permanent[n] = true
		numberSortedNodes--
		sortedNodes[numberSortedNodes] = n
	}

	/*
		Pseudo code from Wikipedia
			while there are unmarked nodes do
			  select an unmarked node n
			  visit(n)
	*/
	for _, n := range start {
		if permanent[n] {
			continue
		}
		visit(n)
		if cycleFound {
			return nil, cyclic
		}
	}

	return sortedNodes, nil
}

// Function to reverse the sorted list for package installation
// The sort will list package to install last, first, therefore reverse
func reverse(sorted []string) {
	last := len(sorted) - 1
	for index, end := range sorted[:len(sorted)/2] {
		sorted[index], sorted[last-index] = sorted[last-index], end
	}
}
